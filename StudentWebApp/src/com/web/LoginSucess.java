package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/LoginSucess")
public class LoginSucess extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");
		
		out.print("<html>");
		out.print("<body bgcolor='lightyellow' text='blue'>");
		out.print("<center>");
		out.print("<form align='right'>");
		out.print("<a href='LoginSucess'>Home</a> &nbsp;");
		out.print("<a href='StudentLogin.html'>Logout</a>");
		out.print("</form>");
		out.print("<h1 style='color:blue'>Welcome to Student Home Page</h1>");	
		out.print("<center></body></html>");
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
