package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");
		
		String StudentId = request.getParameter("studentId");
		String StudentName = request.getParameter("studentName");
		String gender = request.getParameter("gender");
		String emailId = request.getParameter("emailId");
		String password = request.getParameter("Password");
		String number = request.getParameter("studentNumber");
		
		out.print("<html>");
		out.print("<body bgcolor='lightyellow' text='green'>");
		out.print("<center>");
		out.print("<h1>Student Registration</h1>");
		out.print("<h3>StudentID   : " + StudentId + "</h3>");
		out.print("<h3>Student Name : " + StudentName + "</h3>");
		out.print("<h3>Gender  : " + gender + "</h3>");
		out.print("<h3>Email-Id: " + emailId + "</h3>");
		out.print("<h3>Password: " + password + "</h3>");
		out.print("<h3>Mobile number  : " + number + "</h3>");
		out.print("<center></body></html>");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
