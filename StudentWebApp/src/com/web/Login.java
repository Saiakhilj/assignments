package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/Login")
public class Login extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");
		
		String studentId = request.getParameter("studentId");
		String password = request.getParameter("Password");
		
		
		if(studentId.equals("student") && password.equals("123")){
			
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("LoginSucess");
			requestDispatcher.forward(request, response);
		}
		else{
			out.print("<html>");
			out.print("<body");
			out.print("<h1>");
			out.print("Invalid Credentials!!!!!");
			out.print("</h1>");
			out.print("</body>");
			out.print("</html>");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("StudeentLogin.html");
			requestDispatcher.include(request, response);
		}
		
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
